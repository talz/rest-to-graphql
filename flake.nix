{
  inputs = {
    utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/release-21.11";
    unstable.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  };
  outputs = { self, nixpkgs, unstable, utils }:
    utils.lib.eachDefaultSystem (system:
      let 
        pkgs = nixpkgs.legacyPackages.${system};
        unstablePkgs = unstable.legacyPackages.${system};
      in rec {
        defaultPackage = unstablePkgs.rustPlatform.buildRustPackage {
          pname = "graphql-proxy";
          version = "0.1.0";

          src = pkgs.lib.sourceByRegex ./. [
            "^src$"
            ".*\.rs$"
            "^Cargo.toml$"
            "^Cargo.lock$"
          ];

          cargoLock = {
            lockFile = ./Cargo.lock;
          };
        };
        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [
            just
            rustup
          ];
        };
        packages.docker = pkgs.dockerTools.buildImage {
          name = "graphql-proxy";
          contents = defaultPackage;
          # fromImage = pkgs.dockerTools.pullImage {
          #   imageName = "alpine";
          #   imageDigest = "sha256:73c155696fe65b68696e6ea24088693546ac468b3e14542f23f0efbde289cc97";
          #   sha256 = "sha256-wvQOJgJxsbOGqSxYT9Spkhj05b7tgfTfqZU/rlBPQ9Y=";
          # };
          config = {
            Cmd = [ "${defaultPackage}/bin/graphql-proxy" ];
          };
        };
      }
    );
}