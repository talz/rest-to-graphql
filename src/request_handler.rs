use graphql_parser::{query::Query, schema::Type};
use hyper::{Body, Client, Uri, client::HttpConnector, header::HeaderValue};
use hyper_rustls::HttpsConnector;
use route_recognizer::Params;
use tracing::Span;

use crate::query_finder::QueryFinder;

pub type HttpClient = Client<HttpsConnector<HttpConnector>>;

pub struct State {
  pub upstream: Uri,
  pub client: HttpClient,
  pub query_finder: QueryFinder,
  pub openapi_spec_path: Option<String>,
}

pub async fn on_request(
  req: hyper::Request<hyper::Body>,
  state: &State,
  span: Span,
) -> Result<hyper::Response<Body>, RequestHandlerError> {
  let new_request = span.in_scope(||
    transform_query(req, state)
  )?;

  tracing::debug!("Sending request {new_request:?}");

  let response = state.client.request(new_request).await;

  tracing::debug!("Response {response:?}");

  response.map_err(RequestHandlerError::from)
}

fn transform_query(
  req: hyper::Request<hyper::Body>,
  state: &State,
) -> Result<hyper::Request<hyper::Body>, RequestHandlerError> {
  let span = tracing::info_span!("Transform query");
  let _span_guard = span.enter();

  let (query, params) = state.query_finder.find(req.uri().path())
    .map_err(|msg| {
      tracing::error!(%msg, "URL not found");
      RequestHandlerError::UriNotRecognized()
    })?;
  
  let variables = query.with_parsed(|parsed|
    get_variables(&parsed.query, &params)
  )?;

  // *req.uri_mut() = state.upstream.clone();
  // *req.method_mut() = hyper::Method::POST;
  // req.headers_mut().insert(
  //   hyper::header::CONTENT_TYPE,
  //   HeaderValue::from_static("application/json")
  // );
  // *req.body_mut() =
  //   Body::from(
  //     serde_json::json!({
  //       "query": query.borrow_string(),
  //       "variables": variables
  //     })
  //     .to_string()
  //   );
  
  let res = hyper::Request::builder()
    .uri(state.upstream.clone())
    .method(hyper::Method::POST)
    .header(hyper::header::CONTENT_TYPE, HeaderValue::from_static("application/json"))
    .body(Body::from(
      serde_json::json!({
        "query": query.get_normalized(),
        "variables": variables
      })
      .to_string()
    )).map_err(anyhow::Error::from)?;
  
  Ok(res)
}

fn get_variables<'a>(
  query: &Query<'a, &'a str>,
  params: &Params,
) -> Result<serde_json::Value, RequestHandlerError> {
  let mut res = serde_json::Map::new();
  let mut unresolved_variable_names = Vec::<&'a str>::new();

  for variable_def in query.variable_definitions.iter() {
    let name = variable_def.name;
    let value = params.find(variable_def.name);

    match value {
      Some(v) => {
        res.insert(name.to_string(), serde_json::Value::String(v.to_string()));
      },
      None =>
        if let Type::NonNullType(_) = variable_def.var_type {
          unresolved_variable_names.push(name);
        }
    }
  }

  if unresolved_variable_names.len() > 0 {
    return Err(
      RequestHandlerError::MissingVars(
        unresolved_variable_names.into_iter().map(String::from).collect()
      )
    );
  }

  Ok(serde_json::Value::Object(res))
}

#[derive(thiserror::Error, Debug)]
pub enum RequestHandlerError {
  #[error("data store disconnected")]
  HyperError(#[from] hyper::Error),

  #[error("internal")]
  InternalError(#[from] anyhow::Error),

  #[error("URL not recognized")]
  UriNotRecognized(),

  #[error("the data for key `{0:?}` is not available")]
  MissingVars(Vec<String>),
}