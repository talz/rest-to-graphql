use std::path::Path;
use itertools::Itertools;

use graphql_parser::query::{Definition, OperationDefinition};
use route_recognizer::{Router, Params};

pub type GraphqlQuery<'a> = graphql_parser::query::Query<'a, &'a str>;

#[ouroboros::self_referencing]
pub struct Query {
  pub raw: String,

  #[borrows(raw)]
  #[not_covariant]
  pub parsed: ParsedQuery<'this>,
}

impl Query {
  fn from_graphql_query(parsed_query: GraphqlQuery) -> anyhow::Result<Query> {
    let query = QueryTryBuilder {
      raw: parsed_query.to_string(),
      parsed_builder: |string| parse_query(string.as_str()),
    }.try_build()?;
  
    Ok(query)
  }

  pub fn get_normalized(&self) -> &str {
    self.with_parsed(|parsed| parsed.normalized.as_str())
  }
}

pub struct ParsedQuery<'a> {
  pub query: GraphqlQuery<'a>,
  pub config: QueryConfig,
  pub normalized: String,
}

pub struct QueryConfig {
  pub method: hyper::Method,
  pub path: String,
  
}

pub struct QueryFinder {
  queries: Vec<Query>,
  recognizer: route_recognizer::Router<usize>,
}

impl QueryFinder {
  pub fn from_pattern(pattern: &str) -> anyhow::Result<Self> {
    let span = tracing::info_span!("QueryFinder initialization");
    let _span_guard = span.enter();

    let queries: Vec<Query> = 
      glob::glob(pattern)?
        // turn glob errors into anyhow errors
        .map(|res| res.map_err(anyhow::Error::from))
        // extract queries from each path
        .map(|path| read_path_into_queries(path?.as_path()))
        // flatten to a single stream
        .flatten_ok()
        .try_collect()?;
    
    let recognizer = build_recognizer(queries.iter());
    
    let res = QueryFinder {
      queries,
      recognizer,
    };

    tracing::info!("Done");

    Ok(res)
  }

  pub fn find(&self, path: &str) -> Result<(&Query, Params), String> {
    let res = self.recognizer.recognize(path)?;

    let query = self.queries.get(**res.handler()).unwrap();
    let params = res.params();

    Ok((query, params.clone()))
  }

  pub fn list(&self) -> impl Iterator<Item = &Query> {
    self.queries.iter()
  }
}

fn build_recognizer<'a>(
  queries: impl Iterator<Item = &'a Query>
) -> Router<usize> {
  let mut recognizer = route_recognizer::Router::new();
  for (index, query) in queries.enumerate() {
    let path = query.with_parsed(|parsed| parsed.config.path.clone());
    tracing::info!(%path, "Adding query");

    recognizer.add(&path, index)
  }
  recognizer
}

fn read_path_into_queries(path: &Path) -> anyhow::Result<impl IntoIterator<Item = Query>> {
  let str = std::fs::read_to_string(path)?;
  let parsed_queries = find_queries(str.as_str())?;
  let queries: Vec<Query> = parsed_queries.map(Query::from_graphql_query).try_collect()?;
  Ok(queries)
}

fn parse_query<'a>(string: &'a str) -> anyhow::Result<ParsedQuery<'a>> {
  let mut query = find_queries(string)?.next().unwrap();

  let (config, config_index) =
    query
      .directives
      .iter()
      .enumerate()
      .find_map(|(index, directive)|
        match directive_to_query_config(directive) {
          Ok(res) => Some((res, index)),
          _ => None
        }
      )
      .ok_or_else(|| anyhow::anyhow!("No config found in query"))?;

  tracing::debug!("{config_index} {}", query.directives[config_index]);

  query.directives.remove(config_index);

  Ok(ParsedQuery {
    normalized: query.to_string(),
    query,
    config,
  })
}

fn directive_to_query_config<'a, 'b>(
  directive: &'b graphql_parser::query::Directive<'a, &'a str>
) -> anyhow::Result<QueryConfig> {
  if directive.name != "get" {
    anyhow::bail!("Directive name is not recognized");
  }

  let path =
    directive.arguments
      .iter()
      .find_map(|(name, value)|
        match (*name, value) {
          ("path", graphql_parser::query::Value::String(path)) =>
            Some(path.clone()),
          _ => None,
        }
      )
      .ok_or_else(|| anyhow::anyhow!("Couldn't find path argument"))?;

  let query_config = QueryConfig {
    method: hyper::Method::GET,
    path
  };

  Ok(query_config)
}

fn find_queries<'a>(
  queries_string: &'a str
) -> anyhow::Result<impl Iterator<Item = GraphqlQuery<'a>>> {
  let query = graphql_parser::parse_query::<&'a str>(queries_string)?;

  let res = query.definitions
    .into_iter()
    .filter_map(|definition| match definition {
      Definition::Operation(OperationDefinition::Query(q)) =>
        Some(q),
      _ => None
    });

  Ok(res)
}
