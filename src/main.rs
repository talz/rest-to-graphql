use std::net::SocketAddr;
use http::start_server;
use hyper::{Client, Uri};
use query_finder::QueryFinder;
use request_handler::HttpClient;
use clap::Parser;

mod http;
mod request_handler;
mod query_finder;
mod openapi;
mod introspection;

#[derive(clap::Parser)]
#[clap(name = "graphql-proxy")]
struct Opt {
  #[clap(short, long, default_value = "queries/*.graphql")]
  queries_glob: String,

  #[clap(long, default_value = "INFO")]
  log_level: tracing::Level,

  #[clap(subcommand)]
  command: Commands,
}

#[derive(clap::Subcommand)]
enum Commands {
  GenerateOpenapi {
    #[clap(short, long)]
    upstream: Uri,
  },
  Proxy {
    #[clap(short, long)]
    upstream: Uri,

    #[clap(short, long, default_value = "0.0.0.0:9096")]
    listen_address: SocketAddr,

    #[clap(long)]
    openapi_spec_path: Option<String>,
  },
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
  let opts = Opt::parse();

  tracing_subscriber::fmt()
    .with_max_level(opts.log_level)
    .init();
  
  let query_finder = QueryFinder::from_pattern(opts.queries_glob.as_str())?;

  match opts.command {
    Commands::Proxy {upstream, listen_address, openapi_spec_path} =>
      start_server(
        query_finder,
        upstream,
        listen_address,
        openapi_spec_path,
        create_http_client(),
      ).await?,
    Commands::GenerateOpenapi {upstream} => {
      let introspection = introspection::introspect(upstream, &create_http_client()).await?;
      let spec = openapi::generate_openapi_spec(query_finder.list(), &introspection)?;
      serde_yaml::to_writer(std::io::stdout(), &spec)?;
    },
  }

  Ok(())
}

fn create_http_client() -> HttpClient {
  let https = hyper_rustls::HttpsConnectorBuilder::new()
    .with_native_roots()
    .https_or_http()
    .enable_http1()
    .build();
  
  Client::builder().build(https)
}