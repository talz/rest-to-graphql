use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Clone)]
pub struct QueryConfig {
  pub path: String,
  pub query: String,
}