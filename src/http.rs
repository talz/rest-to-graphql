use std::{net::SocketAddr, sync::Arc};

use anyhow::Context;
use hyper::{Server, Body, service::{make_service_fn, service_fn}, Uri};

use crate::{request_handler::{RequestHandlerError, State, on_request, HttpClient}, query_finder::QueryFinder};

pub async fn start_server(
  query_finder: QueryFinder,
  upstream: Uri,
  listen_address: SocketAddr,
  openapi_spec_path: Option<String>,
  client: HttpClient,
) -> anyhow::Result<()> {
  let state = Arc::new(State {query_finder, upstream, openapi_spec_path, client});

  let service = make_service_fn(move |_| futures::future::ready({
    let state = state.clone();
    Ok::<_, hyper::Error>(service_fn(move |req| {
      let state = state.clone();
      handle_request(req, state.clone())
    }))
  }));

  tracing::info!("Listening on {listen_address}");

  let server = Server::bind(&listen_address).serve(service);
  server.await?;

  Ok(())
}

async fn handle_request(
  req: hyper::Request<hyper::Body>,
  state: Arc<State>,
) -> Result<hyper::Response<Body>, hyper::Error> {
  let request_span = tracing::info_span!("Request", path = req.uri().path());
  request_span.in_scope(|| tracing::info!("Received request"));

  if state.openapi_spec_path.as_ref().map(String::as_str) == Some(req.uri().path()) {
    return match generate_openapi_spec(state.as_ref()).await {
      Ok(res) => Ok(res),
      Err(err) => {
        tracing::error!("Failed {err:?}");
        Ok(
          hyper::Response::builder()
            .status(hyper::StatusCode::INTERNAL_SERVER_ERROR)
            .body(Body::from("Error while generating openapi spec"))
            .unwrap()
        )
      }
    };
  }

  let res = on_request(req, state.as_ref(), request_span).await;
  
  match res {
    Ok(res) => Ok(res),
    Err(RequestHandlerError::HyperError(err)) => Err(err),
    Err(RequestHandlerError::UriNotRecognized()) =>
      Ok(
        hyper::Response::builder()
          .status(hyper::StatusCode::NOT_FOUND)
          .body(Body::from("Query not found"))
          .unwrap()
      ),
    Err(RequestHandlerError::MissingVars(_)) => 
      Ok(
        hyper::Response::builder()
          .status(hyper::StatusCode::BAD_REQUEST)
          .body(Body::from("Missing variable"))
          .unwrap()
      ),
    Err(RequestHandlerError::InternalError(_)) =>
      Ok(
        hyper::Response::builder()
          .status(hyper::StatusCode::INTERNAL_SERVER_ERROR)
          .body(Body::from("Internal error"))
          .unwrap()
      )
  }
}

async fn generate_openapi_spec(state: &State) -> anyhow::Result<hyper::Response<Body>> {
  let introspection = crate::introspection::introspect(
    state.upstream.clone(),
    &state.client
  ).await.context("while getting introspection data")?;

  let spec = crate::openapi::generate_openapi_spec(
    state.query_finder.list(),
    &introspection,
  ).context("while generating openapi spec")?;

  let yaml = serde_yaml::to_string(&spec).context("while turning the openapi sepc to YAML")?;

  Ok(
    hyper::Response::builder()
      .header(hyper::header::ACCESS_CONTROL_ALLOW_ORIGIN, "*")
      .body(Body::from(yaml))?
  )
}