use std::collections::BTreeMap;

use itertools::Itertools;
use openapi::{Spec, Info, Operation, Operations, Parameter, Schema, Response};

use crate::{
  query_finder::{Query, ParsedQuery},
  introspection::IntrospectionSchema
};

pub fn generate_openapi_spec<'a>(
  queries: impl Iterator<Item = &'a Query>,
  introspection: &IntrospectionSchema,
) -> anyhow::Result<Spec> {
  Ok(Spec {
    swagger: "2.0".to_string(),
    info: Info {
      title: "".to_string(),
      version: "".to_string(),
      terms_of_service: None,
    },
    paths: queries.map(|query| query.with_parsed(query_to_operation)).try_collect()?,
    definitions: Default::default(),
    schemes: None,
    host: None,
    base_path: None,
    consumes: None,
    produces: None,
    parameters: None,
    responses: None,
    security_definitions: None,
    tags: None,
  })
}

fn query_to_operation(query: &ParsedQuery) -> anyhow::Result<(String, Operations)> {
  let base_operations = Operations {
    get: None,
    post: None,
    put: None,
    patch: None,
    delete: None,
    parameters: None,
  };

  let operations = match query.config.method {
    hyper::Method::GET => Operations {
      get: Some(Operation {
        summary: None,
        description: None,
        consumes: None,
        produces: None,
        schemes: None,
        tags: None,
        operation_id: None,
        responses: [
          ("200".to_string(), Response {
            schema: Some(selection_set_to_openapi_schema(&query.query.selection_set)?),
            description: "".to_string(),
          })
        ].into_iter().collect(),
        parameters: Some(
          query.query.variable_definitions
            .iter()
            .map(graphql_variable_to_openapi_parameter)
            .try_collect()?
        ),
      }),
      ..base_operations
    },
    _ => base_operations
  };

  Ok((query.config.path.to_string(), operations))
}

fn selection_set_to_openapi_schema<'a, T: graphql_parser::schema::Text<'a>>(
  selection_set: &graphql_parser::query::SelectionSet<'a, T>,
) -> anyhow::Result<Schema> {
  use graphql_parser::query::Selection;

  let properties: BTreeMap<String, Schema> = selection_set.items.iter().map(|item|
    match item {
      Selection::Field(field) => {
        Ok((
          field.name.as_ref().to_string(),
          selection_set_to_openapi_schema(&field.selection_set)?
        ))
      },
      Selection::FragmentSpread(fragment_spread) => anyhow::bail!("Err"),
      Selection::InlineFragment(inline_fragment) => anyhow::bail!("Err"),
    }
  ).try_collect()?;

  Ok(Schema {
    properties: Some(properties),
    ..EMPTY_SCHEMA
  })
}

fn graphql_variable_to_openapi_parameter<'a, T: graphql_parser::schema::Text<'a>>(
  variable: &graphql_parser::query::VariableDefinition<'a, T>
) -> anyhow::Result<Parameter> {
  Ok(Parameter {
    format: None,
    location: "path".to_string(),
    name: variable.name.as_ref().to_string(),
    param_type: None,
    required: None,
    schema: Some(graphql_type_to_openapi_schema(&variable.var_type)?),
    unique_items: None,
  })
}

fn graphql_type_to_openapi_schema<'a, T: graphql_parser::schema::Text<'a>>(
  var_type: &graphql_parser::schema::Type<'a, T>
) -> anyhow::Result<Schema> {
  use graphql_parser::schema::Type;
  
  let res = match var_type {
    Type::ListType(t) => {
      let inner = graphql_type_to_openapi_schema(t)?;
      Schema {
        items: Some(Box::new(inner)),
        ..EMPTY_SCHEMA
      }
    },
    Type::NamedType(name) => {
      let schema_type = match name.as_ref() {
        "String" => "string",
        "ID" => "uuid",
        other => anyhow::bail!("Unknown type {}", name.as_ref())
      };
      Schema {
        schema_type: Some(schema_type.to_string()),
        ..EMPTY_SCHEMA
      }
    },
    Type::NonNullType(t) => {
      let inner = graphql_type_to_openapi_schema(t)?;
      Schema {
        ..inner
      }
    },
  };

  Ok(res)
}

const EMPTY_SCHEMA: Schema = Schema {
  ref_path: None,
  description: None,
  schema_type: None,
  format: None,
  enum_values: None,
  required: None,
  items: None,
  properties: None,
};