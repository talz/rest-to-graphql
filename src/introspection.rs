use anyhow::Context;
use hyper::Uri;

use crate::request_handler::HttpClient;

use graphql_introspection_query::introspection_response::IntrospectionResponse;

pub use graphql_introspection_query::introspection_response::{Schema as IntrospectionSchema};

pub async fn introspect(upstream_url: Uri, client: &HttpClient) -> anyhow::Result<IntrospectionSchema> {
  let request_body = serde_json::json!({
    "query": "
      {
        __schema {
          types {
            name
          }
        }
      }
    ".to_string()
  });

  let request = hyper::Request::builder()
    .uri(upstream_url)
    .method(hyper::Method::POST)
    .header(hyper::header::CONTENT_TYPE, "application/json")
    .body(hyper::Body::from(serde_json::to_string(&request_body)?))?;
  
  let response = client.request(request).await.context("while sending request")?;
  let status = response.status();
  let body_bytes = hyper::body::to_bytes(response.into_body()).await.context("while reading the body")?;

  if !status.is_success() {
    anyhow::bail!("Failed to introspect. Received {}: {:?}", status, body_bytes);
  }

  let data: IntrospectionResponse = serde_json::from_slice(body_bytes.as_ref()).context("while parsing the body as JSON")?;

  Ok(
    data
      .into_schema()
      .schema
      .ok_or_else(|| anyhow::anyhow!("Returned data doesn't contain schema"))?
  )
}