start:
  cargo watch -x 'run -- --log-level DEBUG proxy --upstream https://staging.wecare.app/api/graphql --openapi-spec-path /openapi.yaml'

build-docker:
  nix build --show-trace .#docker
  cat result | podman load