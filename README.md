Translates REST requests to GraphQL requests and proxies the requests to a backend.

The idea behind this is that exposing a REST interface can sometimes be more secure and more performant than exposing a GraphQL interface.

### Example

```graphql
query Home @get(path: "/home") {
  featuredPosts {
    title
  }
}
```

The following will expose an HTTP server that responds to `GET /home` requests by proxying to an upstream GraphQL server.

```shell
graphql-proxy --upstream https://MY_GRAPHQL_UPSTREAM
```